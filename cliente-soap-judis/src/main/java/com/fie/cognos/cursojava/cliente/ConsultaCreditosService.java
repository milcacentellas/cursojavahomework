
package com.fie.cognos.cursojava.cliente;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ConsultaCreditosService", targetNamespace = "http://services.judis.fie.com.bo/", wsdlLocation = "http://localhost:8080/wsJudis/ConsultaCreditos?wsdl")
public class ConsultaCreditosService
    extends Service
{

    private final static URL CONSULTACREDITOSSERVICE_WSDL_LOCATION;
    private final static WebServiceException CONSULTACREDITOSSERVICE_EXCEPTION;
    private final static QName CONSULTACREDITOSSERVICE_QNAME = new QName("http://services.judis.fie.com.bo/", "ConsultaCreditosService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/wsJudis/ConsultaCreditos?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        CONSULTACREDITOSSERVICE_WSDL_LOCATION = url;
        CONSULTACREDITOSSERVICE_EXCEPTION = e;
    }

    public ConsultaCreditosService() {
        super(__getWsdlLocation(), CONSULTACREDITOSSERVICE_QNAME);
    }

    public ConsultaCreditosService(WebServiceFeature... features) {
        super(__getWsdlLocation(), CONSULTACREDITOSSERVICE_QNAME, features);
    }

    public ConsultaCreditosService(URL wsdlLocation) {
        super(wsdlLocation, CONSULTACREDITOSSERVICE_QNAME);
    }

    public ConsultaCreditosService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, CONSULTACREDITOSSERVICE_QNAME, features);
    }

    public ConsultaCreditosService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ConsultaCreditosService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns IConsultaCreditos
     */
    @WebEndpoint(name = "ConsultaCreditosPort")
    public IConsultaCreditos getConsultaCreditosPort() {
        return super.getPort(new QName("http://services.judis.fie.com.bo/", "ConsultaCreditosPort"), IConsultaCreditos.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IConsultaCreditos
     */
    @WebEndpoint(name = "ConsultaCreditosPort")
    public IConsultaCreditos getConsultaCreditosPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://services.judis.fie.com.bo/", "ConsultaCreditosPort"), IConsultaCreditos.class, features);
    }

    private static URL __getWsdlLocation() {
        if (CONSULTACREDITOSSERVICE_EXCEPTION!= null) {
            throw CONSULTACREDITOSSERVICE_EXCEPTION;
        }
        return CONSULTACREDITOSSERVICE_WSDL_LOCATION;
    }

}

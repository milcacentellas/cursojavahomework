
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for carteraActualizadaMsg complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="carteraActualizadaMsg">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="raiz" type="{http://services.judis.fie.com.bo/}carteraActualizadaReq" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "carteraActualizadaMsg", propOrder = {
    "raiz"
})
public class CarteraActualizadaMsg {

    protected CarteraActualizadaReq raiz;

    /**
     * Gets the value of the raiz property.
     * 
     * @return
     *     possible object is
     *     {@link CarteraActualizadaReq }
     *     
     */
    public CarteraActualizadaReq getRaiz() {
        return raiz;
    }

    /**
     * Sets the value of the raiz property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarteraActualizadaReq }
     *     
     */
    public void setRaiz(CarteraActualizadaReq value) {
        this.raiz = value;
    }

}

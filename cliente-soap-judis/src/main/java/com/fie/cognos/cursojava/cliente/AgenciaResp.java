
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for agenciaResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="agenciaResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operRespuesta" type="{http://services.judis.fie.com.bo/}agenciaOperRespuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agenciaResp", propOrder = {
    "operRespuesta"
})
public class AgenciaResp {

    protected AgenciaOperRespuesta operRespuesta;

    /**
     * Gets the value of the operRespuesta property.
     * 
     * @return
     *     possible object is
     *     {@link AgenciaOperRespuesta }
     *     
     */
    public AgenciaOperRespuesta getOperRespuesta() {
        return operRespuesta;
    }

    /**
     * Sets the value of the operRespuesta property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgenciaOperRespuesta }
     *     
     */
    public void setOperRespuesta(AgenciaOperRespuesta value) {
        this.operRespuesta = value;
    }

}

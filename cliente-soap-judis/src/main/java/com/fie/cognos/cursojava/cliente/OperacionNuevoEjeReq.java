
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for operacionNuevoEjeReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="operacionNuevoEjeReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="seccOperacion" type="{http://services.judis.fie.com.bo/}operacionNuevoEjeSeccOper" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operacionNuevoEjeReq", propOrder = {
    "seccOperacion"
})
public class OperacionNuevoEjeReq {

    protected OperacionNuevoEjeSeccOper seccOperacion;

    /**
     * Gets the value of the seccOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link OperacionNuevoEjeSeccOper }
     *     
     */
    public OperacionNuevoEjeSeccOper getSeccOperacion() {
        return seccOperacion;
    }

    /**
     * Sets the value of the seccOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperacionNuevoEjeSeccOper }
     *     
     */
    public void setSeccOperacion(OperacionNuevoEjeSeccOper value) {
        this.seccOperacion = value;
    }

}


package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gastosJudiciales complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gastosJudiciales">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="raiz" type="{http://services.judis.fie.com.bo/}gastosJudicialesReq" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gastosJudiciales", propOrder = {
    "raiz"
})
public class GastosJudiciales {

    protected GastosJudicialesReq raiz;

    /**
     * Gets the value of the raiz property.
     * 
     * @return
     *     possible object is
     *     {@link GastosJudicialesReq }
     *     
     */
    public GastosJudicialesReq getRaiz() {
        return raiz;
    }

    /**
     * Sets the value of the raiz property.
     * 
     * @param value
     *     allowed object is
     *     {@link GastosJudicialesReq }
     *     
     */
    public void setRaiz(GastosJudicialesReq value) {
        this.raiz = value;
    }

}

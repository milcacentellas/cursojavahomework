
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for carteraActualizada complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="carteraActualizada">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agenciaactual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estadoactual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechultimopago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldoactual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "carteraActualizada", propOrder = {
    "agenciaactual",
    "estadoactual",
    "fechultimopago",
    "operacion",
    "saldoactual"
})
public class CarteraActualizada {

    protected String agenciaactual;
    protected String estadoactual;
    protected String fechultimopago;
    protected String operacion;
    protected String saldoactual;

    /**
     * Gets the value of the agenciaactual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgenciaactual() {
        return agenciaactual;
    }

    /**
     * Sets the value of the agenciaactual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgenciaactual(String value) {
        this.agenciaactual = value;
    }

    /**
     * Gets the value of the estadoactual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoactual() {
        return estadoactual;
    }

    /**
     * Sets the value of the estadoactual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoactual(String value) {
        this.estadoactual = value;
    }

    /**
     * Gets the value of the fechultimopago property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechultimopago() {
        return fechultimopago;
    }

    /**
     * Sets the value of the fechultimopago property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechultimopago(String value) {
        this.fechultimopago = value;
    }

    /**
     * Gets the value of the operacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * Sets the value of the operacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperacion(String value) {
        this.operacion = value;
    }

    /**
     * Gets the value of the saldoactual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoactual() {
        return saldoactual;
    }

    /**
     * Sets the value of the saldoactual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoactual(String value) {
        this.saldoactual = value;
    }

}

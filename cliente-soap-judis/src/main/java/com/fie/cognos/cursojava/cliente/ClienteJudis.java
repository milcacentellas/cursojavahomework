package com.fie.cognos.cursojava.cliente;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

public class ClienteJudis {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ConsultaCreditosService service = new ConsultaCreditosService();
		IConsultaCreditos cliente = service.getConsultaCreditosPort();

		Map<String, Object> mapRequest = ((BindingProvider) cliente).getRequestContext();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		headers.put("usuario", Collections.singletonList("judisws"));
		headers.put("password", Collections.singletonList("123"));
		mapRequest.put(MessageContext.HTTP_REQUEST_HEADERS, headers);

		List<String> usuarios = headers.get("usuario");

		if (usuarios != null && !usuarios.isEmpty()) {
			
			if (usuarios.get(0).equalsIgnoreCase("judisws")) {

				try {
					OperacionNuevoEjeReq request = new OperacionNuevoEjeReq();
					OperacionNuevoEjeSeccOper operNuevoSecc = new OperacionNuevoEjeSeccOper();
					OperacionNuevoEjeSeccParam operNuevoEjecParam = new OperacionNuevoEjeSeccParam();
					operNuevoEjecParam.setCodOperacion("10000379331");

					System.out.println("Nro de Credito Consultado:         " + operNuevoEjecParam.getCodOperacion());
					System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					operNuevoSecc.setSeccParameters(operNuevoEjecParam);
					request.setSeccOperacion(operNuevoSecc);
					OperacionNuevoEjeResp response = cliente.consultaOpeNuevoEjec(request);

					OperacionNuevoEjeOperRespuesta operacionNuevaEjeRespuesta = response.getOperRespuesta();
					
                   // Respuesta del Servicio
					
					System.out.println("Codigo: " + operacionNuevaEjeRespuesta.getCodResp());
					System.out.println("Descripcion: " + operacionNuevaEjeRespuesta.getDescResp());
					System.out.println("Nro_Operacion: " + operacionNuevaEjeRespuesta.getNroOperacion());

					if (operacionNuevaEjeRespuesta.getCodResp().equals("0000")) {
						
					OperacionNuevoEjeOperRegistro nuevaoperacionejecucion = operacionNuevaEjeRespuesta
							.getSeccRegistros();
					
					if (nuevaoperacionejecucion.getRegistro()!=null && !nuevaoperacionejecucion.getRegistro().isEmpty()) {
						
						//Datos de la Operacion
						imprimirDatosOperacion(nuevaoperacionejecucion.getRegistro());
					 }
					
					}
				} catch (Exception_Exception ex) {
					System.out.println(ex.getMessage());
				}
				
				}else {
					
					System.out.println("Usuario Incorrecto");
				}
			
		}
	}

	private static void imprimirDatosOperacion(List<NuevoEjecucion> registro) {
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		for (NuevoEjecucion item : registro) {

			System.out.println("Datos Cr�dito " + item.toString());

		}

	}

}

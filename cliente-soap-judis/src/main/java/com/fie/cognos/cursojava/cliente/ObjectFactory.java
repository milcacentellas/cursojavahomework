
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fie.cognos.cursojava.cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CarteraActualizadaMsg_QNAME = new QName("http://services.judis.fie.com.bo/", "carteraActualizadaMsg");
    private final static QName _GastosJudiciales_QNAME = new QName("http://services.judis.fie.com.bo/", "gastosJudiciales");
    private final static QName _ConsultaOpeNuevoEjec_QNAME = new QName("http://services.judis.fie.com.bo/", "consultaOpeNuevoEjec");
    private final static QName _ConsultaOpeNuevoEjecResponse_QNAME = new QName("http://services.judis.fie.com.bo/", "consultaOpeNuevoEjecResponse");
    private final static QName _Exception_QNAME = new QName("http://services.judis.fie.com.bo/", "Exception");
    private final static QName _NuevasAgencias_QNAME = new QName("http://services.judis.fie.com.bo/", "nuevasAgencias");
    private final static QName _NuevosEjecucion_QNAME = new QName("http://services.judis.fie.com.bo/", "nuevosEjecucion");
    private final static QName _GastosJudicialesResponse_QNAME = new QName("http://services.judis.fie.com.bo/", "gastosJudicialesResponse");
    private final static QName _CarteraActualizadaMsgResponse_QNAME = new QName("http://services.judis.fie.com.bo/", "carteraActualizadaMsgResponse");
    private final static QName _NuevosEjecucionResponse_QNAME = new QName("http://services.judis.fie.com.bo/", "nuevosEjecucionResponse");
    private final static QName _NuevasAgenciasResponse_QNAME = new QName("http://services.judis.fie.com.bo/", "nuevasAgenciasResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fie.cognos.cursojava.cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ConsultaOpeNuevoEjecResponse }
     * 
     */
    public ConsultaOpeNuevoEjecResponse createConsultaOpeNuevoEjecResponse() {
        return new ConsultaOpeNuevoEjecResponse();
    }

    /**
     * Create an instance of {@link ConsultaOpeNuevoEjec }
     * 
     */
    public ConsultaOpeNuevoEjec createConsultaOpeNuevoEjec() {
        return new ConsultaOpeNuevoEjec();
    }

    /**
     * Create an instance of {@link GastosJudiciales }
     * 
     */
    public GastosJudiciales createGastosJudiciales() {
        return new GastosJudiciales();
    }

    /**
     * Create an instance of {@link CarteraActualizadaMsg }
     * 
     */
    public CarteraActualizadaMsg createCarteraActualizadaMsg() {
        return new CarteraActualizadaMsg();
    }

    /**
     * Create an instance of {@link NuevasAgenciasResponse }
     * 
     */
    public NuevasAgenciasResponse createNuevasAgenciasResponse() {
        return new NuevasAgenciasResponse();
    }

    /**
     * Create an instance of {@link NuevosEjecucionResponse }
     * 
     */
    public NuevosEjecucionResponse createNuevosEjecucionResponse() {
        return new NuevosEjecucionResponse();
    }

    /**
     * Create an instance of {@link CarteraActualizadaMsgResponse }
     * 
     */
    public CarteraActualizadaMsgResponse createCarteraActualizadaMsgResponse() {
        return new CarteraActualizadaMsgResponse();
    }

    /**
     * Create an instance of {@link GastosJudicialesResponse }
     * 
     */
    public GastosJudicialesResponse createGastosJudicialesResponse() {
        return new GastosJudicialesResponse();
    }

    /**
     * Create an instance of {@link NuevosEjecucion }
     * 
     */
    public NuevosEjecucion createNuevosEjecucion() {
        return new NuevosEjecucion();
    }

    /**
     * Create an instance of {@link NuevasAgencias }
     * 
     */
    public NuevasAgencias createNuevasAgencias() {
        return new NuevasAgencias();
    }

    /**
     * Create an instance of {@link AgenciaResp }
     * 
     */
    public AgenciaResp createAgenciaResp() {
        return new AgenciaResp();
    }

    /**
     * Create an instance of {@link OperacionNuevoEjeOperRespuesta }
     * 
     */
    public OperacionNuevoEjeOperRespuesta createOperacionNuevoEjeOperRespuesta() {
        return new OperacionNuevoEjeOperRespuesta();
    }

    /**
     * Create an instance of {@link GastosJudSeccOperacion }
     * 
     */
    public GastosJudSeccOperacion createGastosJudSeccOperacion() {
        return new GastosJudSeccOperacion();
    }

    /**
     * Create an instance of {@link CarteraActualizada }
     * 
     */
    public CarteraActualizada createCarteraActualizada() {
        return new CarteraActualizada();
    }

    /**
     * Create an instance of {@link AgenciaOperRespuesta }
     * 
     */
    public AgenciaOperRespuesta createAgenciaOperRespuesta() {
        return new AgenciaOperRespuesta();
    }

    /**
     * Create an instance of {@link GastosJudicialesResp }
     * 
     */
    public GastosJudicialesResp createGastosJudicialesResp() {
        return new GastosJudicialesResp();
    }

    /**
     * Create an instance of {@link OperacionNuevoEjeOperRegistro }
     * 
     */
    public OperacionNuevoEjeOperRegistro createOperacionNuevoEjeOperRegistro() {
        return new OperacionNuevoEjeOperRegistro();
    }

    /**
     * Create an instance of {@link AgenciaSeccParameters }
     * 
     */
    public AgenciaSeccParameters createAgenciaSeccParameters() {
        return new AgenciaSeccParameters();
    }

    /**
     * Create an instance of {@link Agencia }
     * 
     */
    public Agencia createAgencia() {
        return new Agencia();
    }

    /**
     * Create an instance of {@link NuevosEjecucionReg }
     * 
     */
    public NuevosEjecucionReg createNuevosEjecucionReg() {
        return new NuevosEjecucionReg();
    }

    /**
     * Create an instance of {@link GastosJudicialesRespuesta }
     * 
     */
    public GastosJudicialesRespuesta createGastosJudicialesRespuesta() {
        return new GastosJudicialesRespuesta();
    }

    /**
     * Create an instance of {@link CarteraActualizadaResp }
     * 
     */
    public CarteraActualizadaResp createCarteraActualizadaResp() {
        return new CarteraActualizadaResp();
    }

    /**
     * Create an instance of {@link CarteraActSeccOperacion }
     * 
     */
    public CarteraActSeccOperacion createCarteraActSeccOperacion() {
        return new CarteraActSeccOperacion();
    }

    /**
     * Create an instance of {@link OperacionNuevoEjeSeccOper }
     * 
     */
    public OperacionNuevoEjeSeccOper createOperacionNuevoEjeSeccOper() {
        return new OperacionNuevoEjeSeccOper();
    }

    /**
     * Create an instance of {@link GastosJudicialesRegistro }
     * 
     */
    public GastosJudicialesRegistro createGastosJudicialesRegistro() {
        return new GastosJudicialesRegistro();
    }

    /**
     * Create an instance of {@link NuevosEjecucionSeccOperacion }
     * 
     */
    public NuevosEjecucionSeccOperacion createNuevosEjecucionSeccOperacion() {
        return new NuevosEjecucionSeccOperacion();
    }

    /**
     * Create an instance of {@link AgenciaReq }
     * 
     */
    public AgenciaReq createAgenciaReq() {
        return new AgenciaReq();
    }

    /**
     * Create an instance of {@link CarteraActOperRespuesta }
     * 
     */
    public CarteraActOperRespuesta createCarteraActOperRespuesta() {
        return new CarteraActOperRespuesta();
    }

    /**
     * Create an instance of {@link OperacionNuevoEjeReq }
     * 
     */
    public OperacionNuevoEjeReq createOperacionNuevoEjeReq() {
        return new OperacionNuevoEjeReq();
    }

    /**
     * Create an instance of {@link AgenciaSeccOperacion }
     * 
     */
    public AgenciaSeccOperacion createAgenciaSeccOperacion() {
        return new AgenciaSeccOperacion();
    }

    /**
     * Create an instance of {@link NuevosEjecucionParameters }
     * 
     */
    public NuevosEjecucionParameters createNuevosEjecucionParameters() {
        return new NuevosEjecucionParameters();
    }

    /**
     * Create an instance of {@link NuevosEjecucionReq }
     * 
     */
    public NuevosEjecucionReq createNuevosEjecucionReq() {
        return new NuevosEjecucionReq();
    }

    /**
     * Create an instance of {@link OperacionNuevoEjeResp }
     * 
     */
    public OperacionNuevoEjeResp createOperacionNuevoEjeResp() {
        return new OperacionNuevoEjeResp();
    }

    /**
     * Create an instance of {@link NuevoEjecucion }
     * 
     */
    public NuevoEjecucion createNuevoEjecucion() {
        return new NuevoEjecucion();
    }

    /**
     * Create an instance of {@link CarteraActSeccParameters }
     * 
     */
    public CarteraActSeccParameters createCarteraActSeccParameters() {
        return new CarteraActSeccParameters();
    }

    /**
     * Create an instance of {@link GastosJudicialesReq }
     * 
     */
    public GastosJudicialesReq createGastosJudicialesReq() {
        return new GastosJudicialesReq();
    }

    /**
     * Create an instance of {@link CarteraActualizadaReg }
     * 
     */
    public CarteraActualizadaReg createCarteraActualizadaReg() {
        return new CarteraActualizadaReg();
    }

    /**
     * Create an instance of {@link GastosJudSeccParam }
     * 
     */
    public GastosJudSeccParam createGastosJudSeccParam() {
        return new GastosJudSeccParam();
    }

    /**
     * Create an instance of {@link CarteraActualizadaReq }
     * 
     */
    public CarteraActualizadaReq createCarteraActualizadaReq() {
        return new CarteraActualizadaReq();
    }

    /**
     * Create an instance of {@link AgenciaRegistro }
     * 
     */
    public AgenciaRegistro createAgenciaRegistro() {
        return new AgenciaRegistro();
    }

    /**
     * Create an instance of {@link NuevosEjecucionResp }
     * 
     */
    public NuevosEjecucionResp createNuevosEjecucionResp() {
        return new NuevosEjecucionResp();
    }

    /**
     * Create an instance of {@link OperacionNuevoEjeSeccParam }
     * 
     */
    public OperacionNuevoEjeSeccParam createOperacionNuevoEjeSeccParam() {
        return new OperacionNuevoEjeSeccParam();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarteraActualizadaMsg }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "carteraActualizadaMsg")
    public JAXBElement<CarteraActualizadaMsg> createCarteraActualizadaMsg(CarteraActualizadaMsg value) {
        return new JAXBElement<CarteraActualizadaMsg>(_CarteraActualizadaMsg_QNAME, CarteraActualizadaMsg.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GastosJudiciales }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "gastosJudiciales")
    public JAXBElement<GastosJudiciales> createGastosJudiciales(GastosJudiciales value) {
        return new JAXBElement<GastosJudiciales>(_GastosJudiciales_QNAME, GastosJudiciales.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaOpeNuevoEjec }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "consultaOpeNuevoEjec")
    public JAXBElement<ConsultaOpeNuevoEjec> createConsultaOpeNuevoEjec(ConsultaOpeNuevoEjec value) {
        return new JAXBElement<ConsultaOpeNuevoEjec>(_ConsultaOpeNuevoEjec_QNAME, ConsultaOpeNuevoEjec.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaOpeNuevoEjecResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "consultaOpeNuevoEjecResponse")
    public JAXBElement<ConsultaOpeNuevoEjecResponse> createConsultaOpeNuevoEjecResponse(ConsultaOpeNuevoEjecResponse value) {
        return new JAXBElement<ConsultaOpeNuevoEjecResponse>(_ConsultaOpeNuevoEjecResponse_QNAME, ConsultaOpeNuevoEjecResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevasAgencias }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "nuevasAgencias")
    public JAXBElement<NuevasAgencias> createNuevasAgencias(NuevasAgencias value) {
        return new JAXBElement<NuevasAgencias>(_NuevasAgencias_QNAME, NuevasAgencias.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevosEjecucion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "nuevosEjecucion")
    public JAXBElement<NuevosEjecucion> createNuevosEjecucion(NuevosEjecucion value) {
        return new JAXBElement<NuevosEjecucion>(_NuevosEjecucion_QNAME, NuevosEjecucion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GastosJudicialesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "gastosJudicialesResponse")
    public JAXBElement<GastosJudicialesResponse> createGastosJudicialesResponse(GastosJudicialesResponse value) {
        return new JAXBElement<GastosJudicialesResponse>(_GastosJudicialesResponse_QNAME, GastosJudicialesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarteraActualizadaMsgResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "carteraActualizadaMsgResponse")
    public JAXBElement<CarteraActualizadaMsgResponse> createCarteraActualizadaMsgResponse(CarteraActualizadaMsgResponse value) {
        return new JAXBElement<CarteraActualizadaMsgResponse>(_CarteraActualizadaMsgResponse_QNAME, CarteraActualizadaMsgResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevosEjecucionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "nuevosEjecucionResponse")
    public JAXBElement<NuevosEjecucionResponse> createNuevosEjecucionResponse(NuevosEjecucionResponse value) {
        return new JAXBElement<NuevosEjecucionResponse>(_NuevosEjecucionResponse_QNAME, NuevosEjecucionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevasAgenciasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "nuevasAgenciasResponse")
    public JAXBElement<NuevasAgenciasResponse> createNuevasAgenciasResponse(NuevasAgenciasResponse value) {
        return new JAXBElement<NuevasAgenciasResponse>(_NuevasAgenciasResponse_QNAME, NuevasAgenciasResponse.class, null, value);
    }

}


package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para nuevoEjecucion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="nuevoEjecucion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diasmora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecvenci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="garantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nrolinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responsable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tasa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipooperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ultpago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevoEjecucion", propOrder = {
    "agencia",
    "calificacion",
    "ci",
    "diasmora",
    "estado",
    "fecha",
    "fecvenci",
    "garantia",
    "grupo",
    "moneda",
    "monto",
    "nombre",
    "nrolinea",
    "operacion",
    "prevision",
    "responsable",
    "saldo",
    "tasa",
    "telefono",
    "tipooperacion",
    "ultpago"
})
public class NuevoEjecucion {

    protected String agencia;
    protected String calificacion;
    protected String ci;
    protected String diasmora;
    protected String estado;
    protected String fecha;
    protected String fecvenci;
    protected String garantia;
    protected String grupo;
    protected String moneda;
    protected String monto;
    protected String nombre;
    protected String nrolinea;
    protected String operacion;
    protected String prevision;
    protected String responsable;
    protected String saldo;
    protected String tasa;
    protected String telefono;
    protected String tipooperacion;
    protected String ultpago;

    /**
     * Obtiene el valor de la propiedad agencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Define el valor de la propiedad agencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencia(String value) {
        this.agencia = value;
    }

    /**
     * Obtiene el valor de la propiedad calificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     * Define el valor de la propiedad calificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalificacion(String value) {
        this.calificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad ci.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCi() {
        return ci;
    }

    /**
     * Define el valor de la propiedad ci.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCi(String value) {
        this.ci = value;
    }

    /**
     * Obtiene el valor de la propiedad diasmora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiasmora() {
        return diasmora;
    }

    /**
     * Define el valor de la propiedad diasmora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasmora(String value) {
        this.diasmora = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad fecvenci.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecvenci() {
        return fecvenci;
    }

    /**
     * Define el valor de la propiedad fecvenci.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecvenci(String value) {
        this.fecvenci = value;
    }

    /**
     * Obtiene el valor de la propiedad garantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * Define el valor de la propiedad garantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGarantia(String value) {
        this.garantia = value;
    }

    /**
     * Obtiene el valor de la propiedad grupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Define el valor de la propiedad grupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupo(String value) {
        this.grupo = value;
    }

    /**
     * Obtiene el valor de la propiedad moneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Define el valor de la propiedad moneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Obtiene el valor de la propiedad monto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonto() {
        return monto;
    }

    /**
     * Define el valor de la propiedad monto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonto(String value) {
        this.monto = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad nrolinea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNrolinea() {
        return nrolinea;
    }

    /**
     * Define el valor de la propiedad nrolinea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNrolinea(String value) {
        this.nrolinea = value;
    }

    /**
     * Obtiene el valor de la propiedad operacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * Define el valor de la propiedad operacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperacion(String value) {
        this.operacion = value;
    }

    /**
     * Obtiene el valor de la propiedad prevision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevision() {
        return prevision;
    }

    /**
     * Define el valor de la propiedad prevision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevision(String value) {
        this.prevision = value;
    }

    /**
     * Obtiene el valor de la propiedad responsable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsable() {
        return responsable;
    }

    /**
     * Define el valor de la propiedad responsable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsable(String value) {
        this.responsable = value;
    }

    /**
     * Obtiene el valor de la propiedad saldo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldo() {
        return saldo;
    }

    /**
     * Define el valor de la propiedad saldo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldo(String value) {
        this.saldo = value;
    }

    /**
     * Obtiene el valor de la propiedad tasa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTasa() {
        return tasa;
    }

    /**
     * Define el valor de la propiedad tasa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTasa(String value) {
        this.tasa = value;
    }

    /**
     * Obtiene el valor de la propiedad telefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Define el valor de la propiedad telefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Obtiene el valor de la propiedad tipooperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipooperacion() {
        return tipooperacion;
    }

    /**
     * Define el valor de la propiedad tipooperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipooperacion(String value) {
        this.tipooperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad ultpago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUltpago() {
        return ultpago;
    }

    /**
     * Define el valor de la propiedad ultpago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUltpago(String value) {
        this.ultpago = value;
    }

	@Override
	public String toString() {
		return "NuevoEjecucion [agencia=" + agencia + ", calificacion=" + calificacion + ", ci=" + ci + ", diasmora="
				+ diasmora + ", estado=" + estado + ", fecha=" + fecha + ", fecvenci=" + fecvenci + ", garantia="
				+ garantia + ", grupo=" + grupo + ", moneda=" + moneda + ", monto=" + monto + ", nombre=" + nombre
				+ ", nrolinea=" + nrolinea + ", operacion=" + operacion + ", prevision=" + prevision + ", responsable="
				+ responsable + ", saldo=" + saldo + ", tasa=" + tasa + ", telefono=" + telefono + ", tipooperacion="
				+ tipooperacion + ", ultpago=" + ultpago + "]";
	}

}

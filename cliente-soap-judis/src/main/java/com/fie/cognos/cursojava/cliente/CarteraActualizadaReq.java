
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for carteraActualizadaReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="carteraActualizadaReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="seccOperacion" type="{http://services.judis.fie.com.bo/}carteraActSeccOperacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "carteraActualizadaReq", propOrder = {
    "seccOperacion"
})
public class CarteraActualizadaReq {

    protected CarteraActSeccOperacion seccOperacion;

    /**
     * Gets the value of the seccOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link CarteraActSeccOperacion }
     *     
     */
    public CarteraActSeccOperacion getSeccOperacion() {
        return seccOperacion;
    }

    /**
     * Sets the value of the seccOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarteraActSeccOperacion }
     *     
     */
    public void setSeccOperacion(CarteraActSeccOperacion value) {
        this.seccOperacion = value;
    }

}

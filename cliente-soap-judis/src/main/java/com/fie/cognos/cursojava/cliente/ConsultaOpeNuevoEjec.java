
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultaOpeNuevoEjec complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultaOpeNuevoEjec">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="raiz" type="{http://services.judis.fie.com.bo/}operacionNuevoEjeReq" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaOpeNuevoEjec", propOrder = {
    "raiz"
})
public class ConsultaOpeNuevoEjec {

    protected OperacionNuevoEjeReq raiz;

    /**
     * Gets the value of the raiz property.
     * 
     * @return
     *     possible object is
     *     {@link OperacionNuevoEjeReq }
     *     
     */
    public OperacionNuevoEjeReq getRaiz() {
        return raiz;
    }

    /**
     * Sets the value of the raiz property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperacionNuevoEjeReq }
     *     
     */
    public void setRaiz(OperacionNuevoEjeReq value) {
        this.raiz = value;
    }

}

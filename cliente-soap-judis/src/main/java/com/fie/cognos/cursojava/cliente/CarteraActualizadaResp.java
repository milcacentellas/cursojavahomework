
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for carteraActualizadaResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="carteraActualizadaResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operRespuesta" type="{http://services.judis.fie.com.bo/}carteraActOperRespuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "carteraActualizadaResp", propOrder = {
    "operRespuesta"
})
public class CarteraActualizadaResp {

    protected CarteraActOperRespuesta operRespuesta;

    /**
     * Gets the value of the operRespuesta property.
     * 
     * @return
     *     possible object is
     *     {@link CarteraActOperRespuesta }
     *     
     */
    public CarteraActOperRespuesta getOperRespuesta() {
        return operRespuesta;
    }

    /**
     * Sets the value of the operRespuesta property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarteraActOperRespuesta }
     *     
     */
    public void setOperRespuesta(CarteraActOperRespuesta value) {
        this.operRespuesta = value;
    }

}

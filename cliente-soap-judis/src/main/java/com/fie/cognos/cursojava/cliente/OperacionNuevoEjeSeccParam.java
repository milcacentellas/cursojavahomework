
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for operacionNuevoEjeSeccParam complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="operacionNuevoEjeSeccParam">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operacionNuevoEjeSeccParam", propOrder = {
    "codOperacion"
})
public class OperacionNuevoEjeSeccParam {

    protected String codOperacion;

    /**
     * Gets the value of the codOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOperacion() {
        return codOperacion;
    }

    /**
     * Sets the value of the codOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOperacion(String value) {
        this.codOperacion = value;
    }

}

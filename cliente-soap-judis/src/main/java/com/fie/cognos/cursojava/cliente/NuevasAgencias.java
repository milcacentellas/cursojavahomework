
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nuevasAgencias complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nuevasAgencias">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="raiz" type="{http://services.judis.fie.com.bo/}agenciaReq" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevasAgencias", propOrder = {
    "raiz"
})
public class NuevasAgencias {

    protected AgenciaReq raiz;

    /**
     * Gets the value of the raiz property.
     * 
     * @return
     *     possible object is
     *     {@link AgenciaReq }
     *     
     */
    public AgenciaReq getRaiz() {
        return raiz;
    }

    /**
     * Sets the value of the raiz property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgenciaReq }
     *     
     */
    public void setRaiz(AgenciaReq value) {
        this.raiz = value;
    }

}

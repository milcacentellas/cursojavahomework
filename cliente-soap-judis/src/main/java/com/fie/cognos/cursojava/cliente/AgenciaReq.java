
package com.fie.cognos.cursojava.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for agenciaReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="agenciaReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="seccOperacion" type="{http://services.judis.fie.com.bo/}agenciaSeccOperacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agenciaReq", propOrder = {
    "seccOperacion"
})
public class AgenciaReq {

    protected AgenciaSeccOperacion seccOperacion;

    /**
     * Gets the value of the seccOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link AgenciaSeccOperacion }
     *     
     */
    public AgenciaSeccOperacion getSeccOperacion() {
        return seccOperacion;
    }

    /**
     * Sets the value of the seccOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgenciaSeccOperacion }
     *     
     */
    public void setSeccOperacion(AgenciaSeccOperacion value) {
        this.seccOperacion = value;
    }

}

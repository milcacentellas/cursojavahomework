package com.fie.cognos.cursojava.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import com.fie.cognos.cursojava.entities.Persona;
@WebService
@HandlerChain(file="handler-chain.xml")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class PersonaService {
    @Resource
    WebServiceContext wsc;
    
    
	@WebMethod
	public String saludar(String nombre) throws Exception {
		MessageContext messageContext = wsc.getMessageContext();
		Map <String, Object> headers =(Map<String, Object>)messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
		List<String> usuario =(List<String>)headers.get("usuario");
		List<String> password =(List<String>)headers.get("password");
		if (usuario!=null && password!=null){
		if (usuario.get(0).equals("milca") && password.get(0).equals("3211")) {
		    return "Bienvenido "+usuario.get(0);	
		}
		}
		throw new Exception ("Error de Autorizacion");
		
	}
	
	@WebMethod
	@WebResult(name="persona")
	
	public Persona cargarPersona(@WebParam(name="nombre")String nombrePersona, @WebParam(name="apellidoPaterno")String apellidoPaterno, @WebParam(name="apellidoMaterno")String apellidoMaterno, @WebParam(name="edad")int edad) {
		return new Persona(nombrePersona,apellidoPaterno, apellidoMaterno,edad);
	}
	
	@WebMethod
	@WebResult (name="personas")
	public ArrayList<Persona> listarPersonas(@WebParam(name="nombre")String nombrePersona, @WebParam(name="apellidoPaterno")String apellidoPaterno, @WebParam(name="apellidoMaterno")String apellidoMaterno, @WebParam(name="edad")int edad){
		ArrayList<Persona> personas = new ArrayList<Persona>();
		Persona persona =new Persona();
		persona.setNombre(nombrePersona);
		persona.setApellidoPaterno(apellidoPaterno);
		persona.setApellidoMaterno(apellidoMaterno);
		persona.setEdad(edad);
		personas.add(persona);
		personas.add(persona);
		return personas;
		
	}
	
	@WebMethod
	@WebResult (name="personass")
	public ArrayList<Persona> cargarPersonas(){
		ArrayList<Persona> personas = new ArrayList<Persona>();
		Persona p1 = new Persona("nombre1", "apellido1","apellido1", 1);
		Persona p2 = new Persona("nombre2", "apellido2","apellido2", 2);
		Persona p3 = new Persona("nombre3", "apellido3","apellido3", 3);
		Persona p4 = new Persona("nombre4", "apellido4","apellido4", 4);
		Persona p5 = new Persona("nombre5", "apellido5","apellido5", 5);
		Persona p6 = new Persona("nombre6", "apellido6","apellido6", 6);
		
		personas.add(p1);
		personas.add(p2);
		personas.add(p3);
		personas.add(p4);
		personas.add(p5);
		personas.add(p6);
		return personas;
		
	}
	


	

}

package com.fie.cognos.cursojava.services;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class ValidatorHandler implements SOAPHandler<SOAPMessageContext> {

	@Override
	public void close(MessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("METODO CLOSE");
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("METODO handleFault");
		// este metodo se ejecuta cuando hay error
		return false;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		// este metodo se ejecuta cada vez que se invoca al servicio
		System.out.println("METODO handleMessage");
		this.mostrarMensajes(context);
		if (this.validaDatos(context)) {

		}
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}

	public void mostrarMensajes(SOAPMessageContext context) {
		try {
			boolean isOutBound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

			if (isOutBound) {
				System.out.println("mensaje de salida");
			} else {
				System.out.println("mensaje de entrada");
			}
			SOAPMessage soapMessage = context.getMessage();
			soapMessage.writeTo(System.out);
		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public boolean validaDatos(SOAPMessageContext context) {

		boolean isOutBound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (!isOutBound) {
			System.out.println("mensaje de entrada");

			HttpServletRequest request = (HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST);
			System.out.println("IP: " + request.getRemoteAddr());
			System.out.println("HOST: " + request.getRemoteHost());
			System.out.println("USER: " + request.getRemoteUser());
			System.out.println("Header: " + request.getHeader("milca"));
		}

		return true;
	}
}
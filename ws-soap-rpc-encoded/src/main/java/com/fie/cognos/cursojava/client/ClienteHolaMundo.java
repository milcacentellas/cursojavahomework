package com.fie.cognos.cursojava.client;

import java.util.ArrayList;
import java.util.List;

public class ClienteHolaMundo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PersonaServiceService service = new PersonaServiceService();
		PersonaService cliente = service.getPersonaServicePort();
		Persona persona = cliente.cargarPersona("Milca", "Centellas", "Hinojosa", 38);
		System.out.println("Nombre: "+persona.getNombre()+"\n");
		System.out.println("Apellido Paterno: "+persona.getApellidoPaterno()+"\n");
		System.out.println("Apellido Materno: "+persona.getApellidoMaterno()+"\n");
		System.out.println("Edad: "+persona.getEdad()+"\n");
		List<Persona> listapersonas = cliente.listarPersonas("Jorge", "Candia", "Arce", 30);
		for (Persona per : listapersonas) 
		{ 
			 System.out.println(per.nombre+" "+per.apellidoPaterno+" "+per.apellidoMaterno+" "+per.edad);
		}
       
	}

}


package com.fie.cognos.cursojava.clientes;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fie.cognos.cursojava.clientes package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CargarPersonas_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "cargarPersonas");
    private final static QName _CargarPersona_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "cargarPersona");
    private final static QName _CargarPersonaResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "cargarPersonaResponse");
    private final static QName _Saludar_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "saludar");
    private final static QName _CargarPersonasResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "cargarPersonasResponse");
    private final static QName _ListarPersonas_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "listarPersonas");
    private final static QName _ListarPersonasResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "listarPersonasResponse");
    private final static QName _SaludarResponse_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "saludarResponse");
    private final static QName _Exception_QNAME = new QName("http://services.cursojava.cognos.fie.com/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fie.cognos.cursojava.clientes
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CargarPersonas }
     * 
     */
    public CargarPersonas createCargarPersonas() {
        return new CargarPersonas();
    }

    /**
     * Create an instance of {@link CargarPersona }
     * 
     */
    public CargarPersona createCargarPersona() {
        return new CargarPersona();
    }

    /**
     * Create an instance of {@link CargarPersonaResponse }
     * 
     */
    public CargarPersonaResponse createCargarPersonaResponse() {
        return new CargarPersonaResponse();
    }

    /**
     * Create an instance of {@link Saludar }
     * 
     */
    public Saludar createSaludar() {
        return new Saludar();
    }

    /**
     * Create an instance of {@link CargarPersonasResponse }
     * 
     */
    public CargarPersonasResponse createCargarPersonasResponse() {
        return new CargarPersonasResponse();
    }

    /**
     * Create an instance of {@link ListarPersonas }
     * 
     */
    public ListarPersonas createListarPersonas() {
        return new ListarPersonas();
    }

    /**
     * Create an instance of {@link ListarPersonasResponse }
     * 
     */
    public ListarPersonasResponse createListarPersonasResponse() {
        return new ListarPersonasResponse();
    }

    /**
     * Create an instance of {@link SaludarResponse }
     * 
     */
    public SaludarResponse createSaludarResponse() {
        return new SaludarResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "cargarPersonas")
    public JAXBElement<CargarPersonas> createCargarPersonas(CargarPersonas value) {
        return new JAXBElement<CargarPersonas>(_CargarPersonas_QNAME, CargarPersonas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersona }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "cargarPersona")
    public JAXBElement<CargarPersona> createCargarPersona(CargarPersona value) {
        return new JAXBElement<CargarPersona>(_CargarPersona_QNAME, CargarPersona.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "cargarPersonaResponse")
    public JAXBElement<CargarPersonaResponse> createCargarPersonaResponse(CargarPersonaResponse value) {
        return new JAXBElement<CargarPersonaResponse>(_CargarPersonaResponse_QNAME, CargarPersonaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Saludar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "saludar")
    public JAXBElement<Saludar> createSaludar(Saludar value) {
        return new JAXBElement<Saludar>(_Saludar_QNAME, Saludar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CargarPersonasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "cargarPersonasResponse")
    public JAXBElement<CargarPersonasResponse> createCargarPersonasResponse(CargarPersonasResponse value) {
        return new JAXBElement<CargarPersonasResponse>(_CargarPersonasResponse_QNAME, CargarPersonasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPersonas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "listarPersonas")
    public JAXBElement<ListarPersonas> createListarPersonas(ListarPersonas value) {
        return new JAXBElement<ListarPersonas>(_ListarPersonas_QNAME, ListarPersonas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarPersonasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "listarPersonasResponse")
    public JAXBElement<ListarPersonasResponse> createListarPersonasResponse(ListarPersonasResponse value) {
        return new JAXBElement<ListarPersonasResponse>(_ListarPersonasResponse_QNAME, ListarPersonasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaludarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "saludarResponse")
    public JAXBElement<SaludarResponse> createSaludarResponse(SaludarResponse value) {
        return new JAXBElement<SaludarResponse>(_SaludarResponse_QNAME, SaludarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.cursojava.cognos.fie.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

}

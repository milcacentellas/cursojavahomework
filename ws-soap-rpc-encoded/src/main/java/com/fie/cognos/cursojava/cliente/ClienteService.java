package com.fie.cognos.cursojava.cliente;

import java.util.ArrayList;
import java.util.List;

import com.fie.cognos.cursojava.cliente.Persona;
import com.fie.cognos.cursojava.cliente.PersonaService;
import com.fie.cognos.cursojava.cliente.PersonaServiceService;

public class ClienteService {

	public static void main(String[] args) {
	
		// TODO Auto-generated method stub
				PersonaServiceService service = new PersonaServiceService();
				PersonaService cliente = service.getPersonaServicePort();
				
				List<Persona> listapersonas = cliente.listarPersonas("Jorge", "Candia", "Arce", 30);
				for (Persona per : listapersonas) 
				{ 
					 System.out.println(per.nombre+" "+per.apellidoPaterno+" "+per.apellidoMaterno+" "+per.edad);
				}
				
				List<Persona> lista = cliente.cargarPersonas();
				for (Persona per : lista) 
				{ 
					 System.out.println(per.nombre+" "+per.apellidoPaterno+" "+per.apellidoMaterno+" "+per.edad);
				}
				
				

	}

}

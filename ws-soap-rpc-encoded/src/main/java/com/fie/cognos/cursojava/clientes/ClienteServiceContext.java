package com.fie.cognos.cursojava.clientes;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import com.fie.cognos.cursojava.clientes.Persona;
import com.fie.cognos.cursojava.clientes.PersonaService;
import com.fie.cognos.cursojava.clientes.PersonaServiceService;

public class ClienteServiceContext {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		PersonaServiceService service = new PersonaServiceService();
		PersonaService cliente = service.getPersonaServicePort();

		Map<String, Object> mapRequest=((BindingProvider)cliente).getRequestContext();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		headers.put("usuario", Collections.singletonList("milca"));
		headers.put("password", Collections.singletonList("321"));
		mapRequest.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
		try {
		 System.out.println(cliente.saludar("Milca"));
		 
		}
		catch(Exception_Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		List<Persona> listapersonas = cliente.listarPersonas("Jorge", "Candia", "Arce", 30);
		for (Persona per : listapersonas) 
		{ 
			 System.out.println(per.nombre+" "+per.apellidoPaterno+" "+per.apellidoMaterno+" "+per.edad);
		}
		
		List<Persona> lista = cliente.cargarPersonas();
		for (Persona per : lista) 
		{ 
			 System.out.println(per.nombre+" "+per.apellidoPaterno+" "+per.apellidoMaterno+" "+per.edad);
		}
	
		
		
	}

}

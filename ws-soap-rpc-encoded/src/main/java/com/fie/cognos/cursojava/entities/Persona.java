/**
 * 
 */
package com.fie.cognos.cursojava.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * @author milca.centellas
 *
 */
@XmlRootElement
public class Persona  {
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private int edad;
	
	public Persona() {
		
	}
	//JAXB-Facets
	@XmlElement(required=true)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	
	
	public Persona(String nombre, String apellidoPaterno, String apellidoMaterno, int edadPersona) {
		
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.edad = edadPersona;
	}
	
	

}

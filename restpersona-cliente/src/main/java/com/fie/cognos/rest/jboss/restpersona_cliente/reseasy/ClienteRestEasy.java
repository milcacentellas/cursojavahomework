package com.fie.cognos.rest.jboss.restpersona_cliente.reseasy;




import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.fie.cognos.rest.jboss.restpersona_cliente.entities.Persona;
import com.fie.cognos.rest.jboss.restpersona_cliente.entities.RespuestaServicio;

public class ClienteRestEasy {

	public static void main(String[] args) {

		ResteasyClient cliente = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = cliente.target(UriBuilder.fromPath("http://localhost:8080/rest-persona/rest/"));
		PersonaCliente personaCliente = target.proxy(PersonaCliente.class);

		Response response = personaCliente.getPersona(1L);

		Persona persona = response.readEntity(Persona.class);

		System.out.println(persona.toString());

		Response responseListado = personaCliente.listPersonas();
		
		System.out.println(""+ responseListado);

		if (response.getStatus() == 200) {
			System.out.println("entraa aca");
			try {
				List<Persona> personas = responseListado.readEntity(new GenericType<List<Persona>>() {
				});
				for (Persona item : personas) {
					System.out.println(item);
				}
			} catch (Exception e) {
				RespuestaServicio resp = responseListado.readEntity(RespuestaServicio.class);
				System.out.println(resp.getCodRespuesta() + " - " + resp.getDescripcion());
			}
		} else {
			
			System.out.println("Error al consumir servicio rest");
		}
		
		
		Persona per =new Persona();
		per.setNombre("Juan");
		per.setPaterno("Perez");
		per.setMaterno("Perez1");
		per.setEdad(10);
		String str="?nombre:Milca&paterno:Centellas&materno:Hinojosa&edad:32";
		
		Response responsePost = target.request().post(Entity.entity(per, "/form/?nombre:Milca&paterno:Centellas&materno:Hinojosa&edad:32"));
		//Read output in string format
        System.out.println(response.getStatus());
        response.close();
		
		/* private static void postExample()
		    {*
		        User user = new User();
		        user.setFirstName("john");
		        user.setLastName("Maclane");
		         
		        ResteasyClient client = new ResteasyClientBuilder().build();
		        ResteasyWebTarget target = client.target("http://localhost:8080/RESTEasyApplication/user-management/users");
		        Response response = target.request().post(Entity.entity(user, "application/vnd.com.demo.user-management.user+xml;charset=UTF-8;version=1"));
		        //Read output in string format
		        System.out.println(response.getStatus());
		        response.close(); 
		 /*   }*/
		
	}

}

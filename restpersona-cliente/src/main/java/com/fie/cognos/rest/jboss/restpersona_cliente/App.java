package com.fie.cognos.rest.jboss.restpersona_cliente;



import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import com.fie.cognos.rest.jboss.restpersona_cliente.entities.Persona;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ClientRequest cliente = new ClientRequest("http://localhost:8080/rest-persona/rest/personas");
		try {
			cliente.formParameter("nombre", "sdds");
			ClientResponse<Persona> response = cliente.get(Persona.class);
			Persona persona = response.getEntity();
			
			System.out.println(persona.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

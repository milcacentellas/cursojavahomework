package com.fie.cognos.cursojava.services;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

@WebService
@SOAPBinding(style = Style.RPC, use = Use.LITERAL)
public class RpcLiteralServices {
	@WebMethod
	public String mostrar() {
		return "Ejemplo Rpc/Literal";
	}
}

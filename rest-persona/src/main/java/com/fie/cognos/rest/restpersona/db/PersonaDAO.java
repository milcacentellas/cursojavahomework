package com.fie.cognos.rest.restpersona.db;

import java.util.List;
import org.apache.ibatis.session.SqlSession;

import com.fie.cognos.rest.restpersona.entidades.Persona;

public class PersonaDAO {

	public List<Persona> listPersonas() {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		PersonaMapper mapper = session.getMapper(PersonaMapper.class);
		List<Persona> personas = mapper.listPersonas();
		session.close();
		return personas;
	}

	public Persona getPersona(long id) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		PersonaMapper mapper = session.getMapper(PersonaMapper.class);
		Persona persona = mapper.getPersona(id);
		session.close();
		return persona;
	}

	public boolean insertPersona(Persona persona) {
		try {
			SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
			PersonaMapper mapper = session.getMapper(PersonaMapper.class);
			mapper.insertPersona(persona);
			session.commit();
			session.close();
			return true;	
		} catch(Exception ex) {
			System.out.println(ex);
			return false;
		}
	}
	
	public boolean updatePersona(Persona persona) {
		try {
			SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
			PersonaMapper mapper = session.getMapper(PersonaMapper.class);
			mapper.updatePersona(persona);
			session.commit();
			session.close();
			return true;	
		} catch(Exception ex) {
			System.out.println(ex);
			return false;
		}
	}
	
	public boolean deletePersona(long id) {
		try {
			SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
			PersonaMapper mapper = session.getMapper(PersonaMapper.class);
			mapper.deletePersona(id);
			session.commit();
			session.close();
			return true;	
		} catch(Exception ex) {
			System.out.println(ex);
			return false;
		}
	}
	
}

package com.fie.cognos.rest.restpersona.service;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fie.cognos.rest.restpersona.db.PersonaDAO;
import com.fie.cognos.rest.restpersona.entidades.Persona;
import com.fie.cognos.rest.restpersona.entidades.RespuestaServicio;

@Path("personas")
public class PersonaRest {

	private PersonaDAO personaDAO = new PersonaDAO();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarPersonas(){
		List<Persona> personas = personaDAO.listPersonas();
		if(personas != null && !personas.isEmpty()) {
			System.out.println("Correcto");
			return Response.ok().entity(personas).build();
		} else {
			System.out.println("Error no existen datos");
			//return Response.status(Response.Status.NOT_FOUND).build();	
			//return Response.status(Response.Status.OK).entity(new RespuestaServicio("001", "No existen registros")).build();
			return Response.ok().entity(new RespuestaServicio("001", "No existen registros")).build();
		}
		
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPersona(@PathParam("id") long id) {
		Persona persona = personaDAO.getPersona(id);
		if(persona != null) {
			System.out.println("Correcto");
			return Response.ok().entity(persona).build();
		} else {
			System.out.println("Error no existen la persona");
			return Response.status(Response.Status.NOT_FOUND).build();	
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertPersona(Persona persona) {
		boolean respuesta = personaDAO.insertPersona(persona);
		if(respuesta) {
			System.out.println("Correcto");
			return Response.ok().build();
		} else {
			System.out.println("Error no existen la persona");
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
	
	@POST
	@Path("/form")
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insertarPersona(@FormParam("nombre") String nombre, @FormParam("paterno") String paterno, @FormParam("materno") String materno, @FormParam("edad") int edad) {
		System.out.println("Insert por formparam");
		Persona persona = new Persona();
		persona.setNombre(nombre);
		persona.setPaterno(paterno);
		persona.setMaterno(materno);
		persona.setEdad(edad);
		boolean respuesta = personaDAO.insertPersona(persona);
		if(respuesta) {
			System.out.println("Correcto");
			return Response.ok().build();
		} else {
			System.out.println("Error no existen la persona");
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePersona(Persona persona) {
		if(persona.getId() != 0) {
			boolean respuesta = personaDAO.updatePersona(persona);
			if(respuesta) {
				System.out.println("Correcto");
				return Response.ok().build();
			} else {
				System.out.println("Error no existen la persona");
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}	
		} else {
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePersona(@PathParam("id") long id) {
		boolean respuesta = personaDAO.deletePersona(id);
		if(respuesta) {
			System.out.println("Correcto");
			return Response.ok().build();
		} else {
			System.out.println("Error no existen la persona");
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
	}
	
}

package com.fie.cognos.rest.restpersona.db;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.fie.cognos.rest.restpersona.entidades.Persona;

public interface PersonaMapper {
	@Select("SELECT * FROM persona")
	List<Persona> listPersonas(); 
	
	@Select("SELECT * FROM persona WHERE id = #{id}")
	Persona getPersona(long id);
	
	@Insert("INSERT INTO persona (nombre, paterno, materno, edad) VALUES ( #{nombre}, #{paterno}, #{materno}, #{edad} ) ")
	void insertPersona(Persona persona);
	
	@Update("UPDATE persona SET nombre = #{nombre}, paterno = #{paterno}, materno = #{materno}, edad = #{edad} WHERE id = #{id}")
	void updatePersona(Persona persona);

	@Delete("DELETE FROM persona WHERE id = #{id}")
	void deletePersona(long id);
}

package com.fie.cognos.rest.restpersona.service;

import java.io.IOException;
import java.util.Base64;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class PersonaRestFilter implements ContainerRequestFilter {

	public void filter(ContainerRequestContext requestContext) throws IOException {
		String dato = requestContext.getHeaderString("Authorization");
		String metodo = requestContext.getMethod();
		String uriInfo = requestContext.getUriInfo().getPath();
		System.out.println("Dato: " + dato);
		System.out.println("Metodo: " + metodo);
		System.out.println("UriInfo: " + uriInfo);
		if (dato!= null ){
		String [] datosAuth = dato.split(" ");
		byte[] credenciales = Base64.getDecoder().decode(datosAuth[1]);
		System.out.println(new String(credenciales));
		
		String datoDecodificado = new String(credenciales);
		System.out.println(datoDecodificado);
		String [] datosCredenciales = datoDecodificado.split(":");
		String usuario = datosCredenciales[0];
		String password = datosCredenciales[1];
		
		if(usuario.equals("milca") && password.equals("milca")) {
			System.out.println("Credenciales correctas");
		} else {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
		}
		
	}

}
